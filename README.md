## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.14.0 |
| archive | >=2.2.0 |
| aws | >=3.75.1 |
| null | >=3.1.1 |
| random | >=3.1.3 |

## Providers

| Name | Version |
|------|---------|
| aws | >=3.75.1 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| account\_log\_retention | number of days to store logs | `number` | `90` | no |
| lambda\_architectures | The architecture that Lambda should use.  The values are either arm64 or x86 | `string` | `arm64` | no |
| lambda\_log\_retention | number of days to store logs | `number` | `30` | no |
| lambda\_memory | memory allocation of the processing lambda | `number` | `128` | no |
| lambda\_root | The relative path to the source of the lambda | `string` | `"lambda"` | no |
| lambda\_timeout | timeout for the ami template processing lambda | `number` | `300` | no |
| python\_runtime\_version | The runtime version of Python to be used | `string` | `python3.8` | no | 
| schedule\_expression | CRON expression to invoke the lambda | `string` | `"15 10 * * ? *"` | no |

## Outputs

No output.
