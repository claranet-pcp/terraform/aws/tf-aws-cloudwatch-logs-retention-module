data "aws_iam_policy_document" "retention_policy_permission" {

  statement {
    sid    = "AllowCloudWatchAction"
    effect = "Allow"
    actions = [
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
      "logs:PutRetentionPolicy"
    ]
    resources = ["*"]
  }
}
