from __future__ import print_function

import os
import sys

# Hack to use dependencies from lib directory
BASE_PATH = os.path.dirname(__file__)
sys.path.append(BASE_PATH + "/lib")

import boto3  # nopep8

from aws_lambda_powertools import Tracer, Logger  # nopep8
tracer = Tracer()
logger = Logger(service="Retention Policy")

@tracer.capture_lambda_handler
@logger.inject_lambda_context(log_event=True)
def lambda_handler(event, context):

    client = boto3.client("logs")
    response = client.describe_log_groups()

    for logs in response["logGroups"]:
        logger.debug(logs)

        if "retentionInDays" not in logs:
            retention = int(os.environ["LOG_RETENTION"])
            groupname = logs["logGroupName"]
            logger.info(
                f"setting log rentention to {retention} for group {groupname}")

            client.put_retention_policy(
                retentionInDays=retention,
                logGroupName=groupname,
            )
