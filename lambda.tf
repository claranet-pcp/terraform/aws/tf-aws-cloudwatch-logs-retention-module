module "lambda" {
  source        = "terraform-aws-modules/lambda/aws"
  version       = "3.2.1"
  function_name = "log-retention-policy"
  description   = "Log Retention Policy"
  handler       = "index.lambda_handler"
  runtime       = var.python_runtime_version
  timeout       = var.lambda_timeout
  memory_size   = var.lambda_memory
  architectures = [var.lambda_architectures]

  attach_tracing_policy = true
  source_path = [
    "${path.module}/lambda/index.py",
    {
      pip_requirements = "${path.module}/lambda/requirements.txt"
      prefix_in_zip    = "lib"
    }
  ]

  attach_policy_json = true
  policy_json        = data.aws_iam_policy_document.retention_policy_permission.json
  attach_policies    = true
  tracing_mode       = "Active"

  cloudwatch_logs_retention_in_days = var.lambda_log_retention

  // Add environment variables.
  environment_variables = {
    LOG_RETENTION = var.account_log_retention
  }

  recreate_missing_package = false
  ignore_source_code_hash  = true
}
