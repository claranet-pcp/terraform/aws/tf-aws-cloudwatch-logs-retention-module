resource "aws_cloudwatch_event_rule" "log_retention_policy" {
  name                = "log-retention-policy"
  description         = "Updates with the new retention policy"
  schedule_expression = "cron(${var.schedule_expression})"
}

resource "aws_cloudwatch_event_target" "log_retention_policy" {
  rule = aws_cloudwatch_event_rule.log_retention_policy.name
  arn  = module.lambda.lambda_function_arn
}

resource "aws_lambda_permission" "log_retention_policy" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda.lambda_function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.log_retention_policy.arn
}
