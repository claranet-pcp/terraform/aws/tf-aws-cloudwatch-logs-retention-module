variable "schedule_expression" {
  type        = string
  default     = "15 10 * * ? *"
  description = "CRON expression to invoke the lambda"
}

variable "lambda_timeout" {
  type        = number
  default     = 300
  description = "timeout for the ami template processing lambda"
}

variable "lambda_memory" {
  type        = number
  default     = 128
  description = "memory allocation of the processing lambda"
}

variable "account_log_retention" {
  type        = number
  default     = 90
  description = "number of days to store logs"
}

variable "lambda_log_retention" {
  type        = number
  default     = 30
  description = "number of days to store logs"
}

variable "lambda_root" {
  type        = string
  description = "The relative path to the source of the lambda"
  default     = "lambda"
}

variable "python_runtime_version" {
  type        = string
  default     = "python3.8"
  description = "The runtime version of Python to be used."
}

variable "lambda_architectures" {
  type        = string
  default     = "arm64"
  description = "The architecture that Lambda should use.  The values are either arm64 or x86"
}
